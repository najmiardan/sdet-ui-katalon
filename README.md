# sdet ui katalon

UI Automation Framework using katalon for https://demo.midtrans.com/ website

1. automation test script to test end to end checkout flow for purchasing “Pillow” using Credit Card as payment method. This should be a SUCCESSFUL payment flow.

2. second script to test end to end checkout flow for purchasing “Pillow” using Credit Card as payment method. This should be FAILED payment flow.

3. reporting framework using built-in katalon reporting

4. cross-browser compatible
