package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p>Profile default : Success Credit Card Payment</p>
     */
    public static Object successCard
     
    /**
     * <p>Profile default : Failed Credit Card Payment</p>
     */
    public static Object failCard
     
    /**
     * <p></p>
     */
    public static Object expiryDate
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            successCard = selectedVariables['successCard']
            failCard = selectedVariables['failCard']
            expiryDate = selectedVariables['expiryDate']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
