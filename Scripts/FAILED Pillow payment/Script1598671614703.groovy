import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo.midtrans.com/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('button BUY NOW'))

WebUI.click(findTestObject('button CHECKOUT'))

WebUI.verifyElementClickable(findTestObject('draft continue/button Continue'))

WebUI.click(findTestObject('draft continue/button Continue'))

WebUI.click(findTestObject('Object Repository/a_CreditDebit CardPay with Visa MasterCard _28b126'))

WebUI.setText(findTestObject('Input Card Number'), GlobalVariable.failCard)

WebUI.sendKeys(findTestObject('Input Expiry Date'), GlobalVariable.expiryDate)

WebUI.sendKeys(findTestObject('Input CVV'), '123')

WebUI.click(findTestObject('button Pay Now'))

WebUI.waitForElementPresent(findTestObject('Input Banks OTP'), 3)

WebUI.sendKeys(findTestObject('Input Banks OTP'), '112233')

WebUI.click(findTestObject('button OK'))

WebUI.verifyElementText(findTestObject('draft failed/info Transaction failed'), 'Transaction failed')

WebUI.closeBrowser()

